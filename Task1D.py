
from floodsystem.stationdata import build_station_list
import floodsystem.geo


def main():
	stations = build_station_list()

	river_set = floodsystem.geo.rivers_with_station(stations)

	river_list = list(river_set)
	river_list.sort()
	print(len(river_list))
	print(river_list[:10])


	river_dict = floodsystem.geo.stations_by_river(stations)

	print(river_dict['River Aire'])
	print(river_dict['River Cam'])
	print(river_dict['River Thames'])
	



main()
