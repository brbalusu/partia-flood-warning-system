import matplotlib.pyplot as plt
from datetime import datetime
from floodsystem.station import MonitoringStation
from floodsystem.plot import plot_water_levels, plot_water_level_with_fit

def test_plot_water_levels():
    
    station = MonitoringStation("id_1", "measure_id_1", "label_1", (52.2053, 0.1218), (0.0, 1.0), "river_1", "town_1")
    dates = [datetime(2016, 12, 30), datetime(2016, 12, 31), datetime(2017, 1, 1),
             datetime(2017, 1, 2), datetime(2017, 1, 3), datetime(2017, 1, 4),
             datetime(2017, 1, 5)]
    levels = [0.2, 0.7, 0.95, 0.92, 1.02, 0.91, 0.64]

    plot_water_levels(station, dates, levels)

    #plt.show()

def test_plot_water_level_with_fit():

    station = MonitoringStation("id_1", "measure_id_1", "label_1", (52.2053, 0.1218), (0.0, 1.0), "river_1", "town_1")
    dates = [datetime(2016, 12, 30), datetime(2016, 12, 31), datetime(2017, 1, 1),
             datetime(2017, 1, 2), datetime(2017, 1, 3), datetime(2017, 1, 4),
             datetime(2017, 1, 5)]
    levels = [0.2, 0.7, 0.95, 0.92, 1.02, 0.91, 0.64]

    plot_water_level_with_fit(station, dates, levels, 4)

