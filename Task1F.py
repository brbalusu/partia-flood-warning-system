from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations
def main():

	stations = build_station_list()

	inconsistent_list = inconsistent_typical_range_stations(stations)

	list_names = []

	for i in inconsistent_list:
		list_names.append(i.name)


	print(sorted(list_names))



main()

	

