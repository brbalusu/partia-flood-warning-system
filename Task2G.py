from floodsystem.stationdata import build_station_list
from floodsystem.flood import flood_risk
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels, plot_water_level_with_fit
import datetime
import matplotlib.pyplot as plt

def main():
    """Requirements for Task 2G"""
    
    # Build list of stations
    stations = build_station_list()

    risk_analysis = flood_risk(stations)

    popped_stations = 0
    for i in range(len(risk_analysis)):
        if risk_analysis[i-popped_stations][1] == None:
            risk_analysis.pop(i-popped_stations)
            popped_stations += 1


    risk_analysis.sort(key=lambda tup: tup[1], reverse=True)

    high_risk = risk_analysis[:10]

    print("\n")

    for high_risk_station in high_risk:
        print(high_risk_station[0].name, "    Risk factor =", round(high_risk_station[1], 4), "    Risk level: ", high_risk_station[2])




main()
