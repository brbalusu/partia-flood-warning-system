# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
"""def sorted_by_key(x, i, reverse=False):
    For a list of lists/tuples, return list sorted by the ith
    component of the list/tuple, E.g.

    Sort on first entry of tuple:

      > sorted_by_key([(1, 2), (5, 1]), 0)
      >>> [(1, 2), (5, 1)]

    Sort on second entry of tuple:

      > sorted_by_key([(1, 2), (5, 1]), 1)
      >>> [(5, 1), (1, 2)]

    

    # Sort by distance
    def key(element):
        return element[i]

    return sorted(x, key=key, reverse=reverse)"""

def stations_by_distance(stations, p):

	from floodsystem.haversine import haversine
	from floodsystem.utils import sorted_by_key
	dist = []

	for i in stations:

		dist.append(haversine(i.coord, p))
	
	SortedList = sorted_by_key(zip(stations,dist), 1)
    

	return SortedList


def stations_within_radius(stations, centre, r):

	station_list = []
	

	sorted_stations = stations_by_distance(stations, centre)
	for i in range(len(sorted_stations)):
	
		if (sorted_stations[ i ][ 1 ] <= r):
			station_list.append(sorted_stations[ i ][0])
		else:
			break

	return station_list



def rivers_with_station(stations):

	river_list = set()

	for i in stations:

		river_list.add(i.river)

	return river_list


def stations_by_river(stations):

	"""Take list of stations, and return dictionary with river names as keys"""
	rivers_and_stations = {}

	for i in range(0, len(stations)-1):

	
		rivers_and_stations.setdefault(stations[i].river, []).append(stations[i].name)
	rivers_and_statsions = sorted(rivers_and_stations)
	return rivers_and_stations


def rivers_by_station_number(stations, N):
	from floodsystem.utils import sorted_by_key
	river_list = []
	river_lists =[]
	river = []
	for i in stations:
		river.append(i.river)
	
	for j in range(len(river)):

		if river[j] not in river_lists:
			
			n = 0
			for k in range(j, len(river)):
				if river[k] == river[j]:
					n = n + 1
			river_list.append((river[j], n))
			river_lists.append(river[j])

		else:
			continue

	river_list = sorted_by_key(river_list, 1, True)


	limit = river_list[N-1][1]

	rlist =[]

	for z in river_list:
		if z[1] >= limit:
			rlist.append(z)
		else:
			break
			


	return rlist


