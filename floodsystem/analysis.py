
import matplotlib, datetime
import numpy as np
from .stationdata import update_water_levels
from .datafetcher import fetch_measure_levels
import matplotlib.pyplot as plt
from datetime import timedelta

def polyfit(dates, levels, p):
    """Compute a least-squares fit of a polynomial
    of degree p to water level data (from dates,
    levels) and returns a numpy.poly1d object of the 
    polynomial coefficents and a datetime axis shift.
    """

    t = matplotlib.dates.date2num(dates)

    if len(t) > 0 and len(levels) > 0 and len(t) == len(levels):

        # Find coefficients of best-fit polynomial f(x) of degree p
        p_coeff = np.polyfit(t - t[0], levels, p)
        p_coeff = tuple(p_coeff)

        poly = np.poly1d(p_coeff)

        return poly, t[0]

    else:
        return None, None

