# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        d += "   latest level: {}".format(self.latest_level)
        return d

    def typical_range_consistent(self):

	

        if self.typical_range == None:
            return False
	
	
        elif self.typical_range[1] < self.typical_range[0]:
            return False
	
        else:
            return True

    def risk_level(self):
	
        from floodsystem.datafetcher import fetch_measure_levels
        from floodsystem.analysis import polyfit
        import numpy as np
        import datetime
        import matplotlib.dates
        dates, levels = fetch_measure_levels(self.measure_id, dt = datetime.timedelta(days =10))
        poly = polyfit( dates, levels, 4)
        polyd = np.polyder( poly, 1)

        rel = self.relative_water_level()

        if rel > 1 :
            return "severe"
        elif rel < 1 and rel > 0.8:
            return "high"
        elif rel > 0.6 and rel < 0.8:
            return "medium"
        else:
            return "low"


    def relative_water_level(self):
        if not self.typical_range_consistent() or not self.latest_level:
            return None

        elif self.latest_level == None:
             return None
        else:
            range = self.typical_range[1] - self.typical_range[0]

            fraction = (self.latest_level - self.typical_range[0]) / range

            return fraction


def inconsistent_typical_range_stations(stations):
    inconsistent_list = []
    for i in stations:
        if not i.typical_range_consistent():
            inconsistent_list.append(i)

    return inconsistent_list

