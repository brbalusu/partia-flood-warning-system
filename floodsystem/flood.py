from floodsystem.stationdata import update_water_levels
from floodsystem.utils import sorted_by_key
from floodsystem.stationdata import build_station_list
from floodsystem.analysis import polyfit
from floodsystem.datafetcher import fetch_measure_levels
import datetime
from floodsystem.station import MonitoringStation
import numpy as np
import matplotlib.dates

def stations_level_over_threshold(stations, tol):

	over_threshold = list()


	for station in stations:	

		if station.latest_level and station.typical_range_consistent():
			if station.latest_level > station.typical_range[1]:
				station_over_data = (station.name, station.latest_level)
				over_threshold.append(station_over_data)


	return over_threshold

def stations_highest_rel_level(stations, N):

	stations_rel_level = []


	for station in stations:
		if station.relative_water_level():
			relative_level = station.relative_water_level()
			stations_rel_level.append((station, relative_level))

	stations_rel_level.sort(key=lambda tup: tup[1], reverse=True)

	return stations_rel_level[:N]

def flood_risk(stations):
    "Find risk of flooding at stations passed to function"

    stations_with_risk = []

    update_water_levels(stations)

    for station in stations:

        risk = 0

        if station.latest_level != None and station.relative_water_level() != None and station.typical_range_consistent():

            if station.relative_water_level() > 0.8:

                risk_category, risk = find_risk_for_station(station)

            elif station.relative_water_level() > 0.6:

                risk_category = "Moderate"

            else:

                risk_category = "Low"

        try:
            risk
        except NameError:
            risk = None

        stations_with_risk.append((station, risk, risk_category))

        #print(station.name, risk, risk_category)

    return stations_with_risk

def find_risk_for_station(station):

    risk = 0

    risk += station.relative_water_level() - 0.8

    dt = 5
    p = 1
    dates, levels = fetch_measure_levels(station.measure_id,
                                            dt=datetime.timedelta(days=dt))

    if len(dates) > 0:
        poly, d = polyfit(dates, levels, p)
    else:
        print("Dates list empty")
        poly = None


    if poly != None:
        if len(poly.c) >= 2:
            gradient1 = np.polyder(poly)
            gradient = gradient1(matplotlib.dates.date2num(dates[-1]) - d)
            print("Gradient = ", gradient)
        else:
            print("Polynomial has insufficient coefficients")
            gradient = None
    else:
        print("No polynomial found")
        gradient = None

    if gradient != None:
        risk += gradient

    print("Risk = ", risk)

    risk_category = ""

    if risk != None:
        if risk > 1:
            risk_category = "Severe"
        elif risk > 0.5:
            risk_category = "High"
        else:
            risk_category = "Moderate"
    else:
        risk_category = "Low"

    print("Risk category = ", risk_category)

    return risk_category, risk

