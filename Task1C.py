from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

def main():

	stations = build_station_list()

	test_coord = (52.2053, 0.1218)

	river_list = stations_within_radius(stations, test_coord, 10)

	names = []
	for o in range(len(river_list)):

		names.append(river_list[o].name)
	
	print(sorted(names))



main()