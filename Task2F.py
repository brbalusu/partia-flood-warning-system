import operator, datetime
import matplotlib.pyplot as plt
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels

def main():
    """Requirements for Task 2F"""

    stations = build_station_list()
    update_water_levels(stations)

    # Remove stations with None value for height
    popped_stations = 0
    for i in range(len(stations)):
        if stations[i-popped_stations].relative_water_level() == None:
            stations.pop(i-popped_stations)
            popped_stations += 1
        
    # Sort by latest level
    stations.sort(key=lambda obj: obj.relative_water_level(), reverse=True)

    stations = stations[:5]

    for station in stations:
        dt = 2
        dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))

        plt.figure()

        plot_water_level_with_fit(station, dates, levels, 4)

    plt.show()



main()