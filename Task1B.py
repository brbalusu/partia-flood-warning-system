from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

def main():

	stations = build_station_list()

	camcoord = (52.2053, 0.1218)
	SortedTup = stations_by_distance(stations, camcoord)
	
	result =[]
	
	for i in range(len(stations)):
		result.append((SortedTup[i][0].name, SortedTup[i][0].town, SortedTup[i][1]))

	print(result[0:10])
	print(result[-10:])



main()

	
