from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels
from floodsystem.datafetcher import fetch_measure_levels
import datetime

num_of_stations = 5
over_last_past_days = 10



stations = build_station_list()
update_water_levels(stations)
stations_with_high_rel_level = stations_highest_rel_level(stations, num_of_stations)


for station, level in stations_with_high_rel_level:
	dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=over_last_past_days))

	plot_water_levels(station, dates, levels)