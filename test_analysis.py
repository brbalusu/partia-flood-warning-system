from floodsystem.analysis import polyfit
from datetime import datetime

def test_polyfit():

    dates = [datetime(2017, 1, 3), datetime(2017, 1, 4), datetime(2017, 1, 5)]
    levels = [1, 2, 3]

    poly = polyfit(dates, levels, 2)

    assert round(poly[0].c[-2],1) == 1


