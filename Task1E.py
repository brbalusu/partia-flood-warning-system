from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number


def main():

	stations = build_station_list()


	rivers = rivers_by_station_number(stations, 9)


	print(rivers)



main()
